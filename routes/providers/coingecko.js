const CoinGecko = require('coingecko-api');

const CoinGeckoClient = new CoinGecko();

const BASE = 'usd';

module.exports =
  /**
   *
   * @returns {Promise<Array<Object>>}
   */
  async (limit, date) => {
    let result = [];

    let markets = await CoinGeckoClient.coins.markets({vs_currency: BASE, per_page: limit, page: 0});
    if (markets.code !== 200)
      throw new Error(result.message);

    markets.data.forEach(el =>
      result.push({symbol: el.symbol, currentPrice: el.current_price}));

    if (date) {
      await Promise.all(markets.data.map(async (el, index) => {
        let history = await CoinGeckoClient.coins.fetchHistory(markets.data[index].id, {date});
        if (history.code !== 200)
          throw new Error(history.message);

        result[index].price = history.data.market_data.current_price[BASE];
      }));
    }

    return result;
  };
