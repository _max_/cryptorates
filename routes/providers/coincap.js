const moment = require('moment');
const got = require('got');
const DATE_FORMAT = require('../constants').DATE_FORMAT;

module.exports =
  /**
   *
   * @returns {Promise<Array<Object>>}
   */
  async (limit, date) => {
    let result = [];

    let assets = await got('http://api.coincap.io/v2/assets', {searchParams: {limit}}).json();

    assets.data.forEach(el => result.push({symbol: el.symbol, currentPrice: +el.priceUsd}));

    if (date) {
      await Promise.all(assets.data.map(async (el, index) => {
        let d = moment(date, DATE_FORMAT);
        let history = await got(`http://api.coincap.io/v2/assets/${assets.data[index].id}/history`, {
          searchParams: {
            interval: 'd1',
            start: d.startOf('day').format('x'),
            end: d.endOf('day').format('x')
          }
        }).json();

        result[index].price = history.data[0] ? +history.data[0].priceUsd : null;
      }));
    }

    return result;
  };

