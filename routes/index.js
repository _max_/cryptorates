const express = require('express');
const moment = require('moment');
const router = express.Router();
const DATE_FORMAT = require('./constants').DATE_FORMAT;

const MAX_LIMIT = 100;
const DEFAULT_LIMIT = 10;
const PROVIDERS = {
  'COINGECKO': 'coingecko',
  'COINCAP': 'coincap'
};

const coingeckoProvider = require('./providers/coingecko');
const coincapProvider = require('./providers/coincap');

router.get('/prices', async (req, res) => {
  let limit = +req.query.limit;
  let date = req.query.date;

  try {
    if (!Number.isSafeInteger(limit) || limit > MAX_LIMIT || limit <= 0)
      throw new Error('invalid limit');

    if (date) {
      if (!moment(date, DATE_FORMAT, true).isValid())
        throw new Error('invalid date');
    }
  } catch (e) {
    res.status(400).send(e.message);
    return;
  }

  let provider;
  switch (req.query.provider) {
    case PROVIDERS.COINCAP:
      provider = coincapProvider;
      break;
    case PROVIDERS.COINGECKO:
    default:
      provider = coingeckoProvider;
      break;
  }

  try {
    res.json(await provider(limit || DEFAULT_LIMIT, date));
  } catch (e) {
    res.status(500).send(`provider error: ${e.message}`);
  }
});

module.exports = router;
