# CryptoRates

Show top crypto prices from CoinGecko or CoinCap

You can try it here: https://cryptorates-tc843.ondigitalocean.app/prices?limit=10

**URL** : `/prices?date={'DD-MM-YYYY'}&limit={0<int<=100}&provider={'coingecko'|'coincap'}`

**Method** : `GET`

## Responses:

### Invalid date or limit:

**Code** : `400`

**Content** : `'error message'`

### Error from a data provider:
**Code** : `500`

**Content** : `'provider error: {message}'`

### Success

**Condition** : All parameters are valid and data was received.

**Code** : `200 `

**Content** : 

Example for `/prices?date=30-12-2020&limit=2&provider=coingecko`

```json
[
  {
    "symbol": "btc",
    "currentPrice": 54960,
    "price": 27424.538954606684
  },
  {
    "symbol": "eth",
    "currentPrice": 2737.99,
    "price": 735.5908981625738
  }
]
```
